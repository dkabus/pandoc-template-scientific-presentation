---
date: 2025-02-19
title: Example presentation
author: Desmond Kabus
institute:
- \texttt{d.kabus@lumc.nl}
- \includegraphics[width=40mm]{design/logos.png}
- Laboratory of Experimental Cardiology
aspectratio: 169
classoption:
- fleqn
header-includes: |
    ```{=latex}
    \usepackage[round-mode=uncertainty,round-precision=2,separate-uncertainty=true]{siunitx}
    \usepackage{emoji}
    \setemojifont{Twemoji}
    \useinnertheme{circles}
    \setbeamertemplate{navigation symbols}{}
    \setbeamertemplate{footline}[frame number]
    \usebackgroundtemplate{\includegraphics[width=\paperwidth,height=\paperheight]{design/bg.png}}
    \newcommand{\avg}[1]{{\left\langle#1\right\rangle}}
    \newcommand{\abs}[1]{{\left|#1\right|}}
    \newcommand{\norm}[1]{{\left\lVert#1\right\rVert}}
    \newcommand{\br}[1]{{\left[#1\right]}}
    \newcommand{\brc}[1]{{\left\{#1\right\}}}
    \newcommand{\brr}[1]{{\left(#1\right)}}
    \renewcommand{\exp}[1]{{\mathrm{e}^{#1}}}
    \renewcommand{\phi}[0]{\varphi}
    \newcommand{\transpose}[1]{{#1}^\mathrm{T}}
    \newcommand{\dd}[0]{\mathrm{d}}
    \usepackage{caption}
    \captionsetup[figure]{labelformat=empty}
    ```
...

# 📊 Welcome

Welcome to my presentation.
[@kabus2022numerical]

$$
y = e^x
$${#eq:test}

References: @eq:test.

![The logo](logo.png)

# 📚 References {.allowframebreaks}
