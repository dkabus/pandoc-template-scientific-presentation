auxdir=/tmp/latexmk/$(subst /,%,$(PWD))

all: main.pdf main.tex

luafilters := $(wildcard filters/*.lua)
sedfilters := $(wildcard filters/*.sed)
%.tex: %.md lit.bib style.csl $(luafilters) $(sedfilters)
	pandoc -s -t beamer --slide-level=2 \
		--filter=pandoc-crossref \
		--citeproc --csl=$(word 3,$^) --bibliography=$(word 2,$^) \
		$(addprefix --lua-filter=,$(luafilters)) \
		-o $@ $<
	echo $(sedfilters) | xargs -rI {} sed -f {} -i $@

%.pdf: %.tex
	mkdir -p -m700 '$(auxdir)'
	latexmk -aux-directory='$(auxdir)' -pdflua -interaction=nonstopmode -f -time $<

clean:
	$(RM) main.tex main.pdf
	$(RM) -r '$(auxdir)'
