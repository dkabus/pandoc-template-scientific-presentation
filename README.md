# Pandoc template for a scientific presentation

Markdown/TeX source code for a scientific presentation.

## Requirements to compile

You may need to first download all submodules included in this repository:
```{=sh}
$ git submodule update --init --recursive
```

Just compile with `make`:
```{=sh}
$ make
```

You need these tools installed:

- [GNU Make](https://www.gnu.org/software/make/)
- [Pandoc](https://pandoc.org/)
- a full [LaTeX installation](https://wiki.archlinux.org/title/TeX_Live) with:
    - `latexmk`
    - `pdflatex`
    - `biblatex`
    - `natbib`

## Files

- [`main.md`](main.md): Markdown source file. Edit this file!
- [`main.tex`](main.tex): LaTeX code from compiling the Markdown file via Pandoc.
- [`main.pdf`](main.pdf): Resulting PDF file from compiling the LaTeX file.

## Compilation via Gitlab CI

This repository contains configuration to automatically compile the document
and publish it via Gitlab Pages. You can find the pages here:

<https://dkabus.gitlab.io/pandoc-template-scientific-presentation/>

<https://gitlab.com/dkabus/pandoc-template-scientific-presentation/>
